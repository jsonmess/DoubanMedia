//
//  DMMeiZiConstant.h
//  DoubanMedia
//
//  Created by jsonmess on 15/4/28.
//  Copyright (c) 2015年 jsonmess. All rights reserved.
//

#ifndef DoubanMedia_DMMeiZiConstant_h
#define DoubanMedia_DMMeiZiConstant_h

static NSString * const BASE_URL         = @"http://www.dbmeinv.com";
static NSString * const PIC_HOST         = @"http://123.56.144.212/";
static NSString * const MEIZI_ALL        = @"/dbgroup/show.htm";
static NSString * const MEIZI_SEX        = @"/dbgroup/show.htm?cid=1";
static NSString * const MEIZI_CLEAVAGE   = @"/dbgroup/show.htm?cid=2";//胸
static NSString * const MEIZI_LEGS       = @"/dbgroup/show.htm?cid=3";
static NSString * const MEIZI_FRESH      = @"/dbgroup/show.htm?cid=4";//
static NSString * const MEIZI_LITERATURE = @"/dbgroup/show.htm?cid=5";//文艺
static NSString * const MEIZI_CALLIPYGE  = @"/dbgroup/show.htm?cid=6";//臀
static NSString * const MEIZI_FUNNY      = @"/m/category_new/9";//其他
static NSString * const MEIZI_RATING     = @"/dbgroup/show.htm?cid=7";//尺度

#endif
