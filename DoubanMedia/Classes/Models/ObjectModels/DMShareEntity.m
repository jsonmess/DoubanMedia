//
//  DMShareEntity.m
//  ShareDemo
//
//  Created by jsonmess on 15/4/26.
//  Copyright (c) 2015年 jsonmess. All rights reserved.
//

//分享图片的类型
enum ThumbnailType
{
    kThumbnailTypeJPG = 0,
    kThumbnailTypeJPEG,
    kThumbnailTypePNG
};
#import "DMShareEntity.h"

@implementation DMShareEntity

@end
