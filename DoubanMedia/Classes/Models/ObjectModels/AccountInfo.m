//
//  AccountInfo.m
//  DoubanMedia
//
//  Created by jsonmess on 15/3/26.
//  Copyright (c) 2015年 jsonmess. All rights reserved.
//

#import "AccountInfo.h"


@implementation AccountInfo

@dynamic banned;
@dynamic cookies;
@dynamic isNotLogin;
@dynamic liked;
@dynamic name;
@dynamic played;
@dynamic userId;

@end
