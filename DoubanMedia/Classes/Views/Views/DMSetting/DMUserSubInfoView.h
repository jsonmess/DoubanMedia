//
//  DMUserSubInfoView.h
//  DoubanMedia
//
//  Created by jsonmess on 15/5/4.
//  Copyright (c) 2015年 jsonmess. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMUserSubInfoView : UIView

//图标
-(UIImageView*)getImageView;

//标题
-(UILabel *)getInfoTextLabel;

@end
