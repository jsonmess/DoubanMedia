//
//  TabItem.h
//  DoubanMedia
//
//  Created by jsonmess on 15/3/22.
//  Copyright (c) 2015年 jsonmess. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabItem : UIButton
-(void)SetButtonview:(NSString *)title NomalImage:(NSString *)nomal_image SelectedImage :(NSString *)selected_image;
@end
